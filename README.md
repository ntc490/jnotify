About jnotify

Send SMS, Email, or Growl notifications after a process completes.  Get
something to snack on while your build is going without worrying about wasting
time.  You can receive an alert when it finishes with jnotify.

Requires the following:
  - googlevoice: only if using Google SMS for notifications.  Use your Google
    Voice number for SMS messaging.
  - gntp: only required when using Growl notifications. See
    https://github.com/kfdm/gntp/

The googlevoice module fails to login.  Grab a fixed version at:
  hg clone https://code.google.com/r/bwpayne-pygooglevoice-auth-fix


HOWTO Use Jnotify

There are two main ways to use jnotify.  Use it like the GNU 'time'
command, or to "watch" an existing process.

Using jnotify to issue a new command requires less typing and allows
you to receive the process exit code in completion notifications.
Sometimes it's hard to remember to use jnotify when you start a new
command.

You may wish to monitor a process that is already running.  This tool
can be used to watch another user's command, one that you started a
while ago, or even one on another host.  Use the -p or --process
argument to specify a process to watch.  You can use a substring of
the process 'command' or a pid.

Remote watches use SSH as transport.  You will likely want to use key
authentication with the remote host to avoid a lot of interactive
prompting.

Ambiguous process descriptions are resolved by picking the youngest
instance by default.  If you wish to watch an older instance, use the
pid instead of the name.

The jnotify server can be used to cache notifier 'preparation'.  A good
example is Gmail SMS login credentials.  While you can include the
Gmail password in the config file, it's not recommended.  Start the
jnotify server, login to Google's servers, and future tool utilization
will leverage the server to send notifications.

A named pipe is used for client/server communication, so others won't
be able to easily poach your Gmail account to send SMS messages.  The
server is protected by standard Unix file permissions.

The config file is ~/.jnotify.  See the SETTINGS_*_TOKEN vars for
possible configuration options.

Here are some example usage scenarios.

  # Invoke 'make all' and provide Growl notification when done
  jnotify --growl make all

  # Use '--' to mark the end of jnotify switches and start with
  # switches for the new command.
  jnotify --smtp -- rsync -avz here there

  # Creating a server for Google SMS notifications
  jnotify -d --google-sms

  # Watch another user's rsync command.  Use FIFO server if it's running or the
  # default notifier type (set in config file).
  jnotify --user wheel -p rsync

  # Use Growl to monitor a process on another host.
  jnotify --growl --host gibson -p mplayer

  # Use quotes with the -p switch to make searches more specific.
  # Recursive make necessitates this quite often as younger instances
  # are probably not what you want.
  jnotify --process 'make initramfs'
